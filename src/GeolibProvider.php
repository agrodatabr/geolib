<?php

namespace Agrodata\Geolib;

use Illuminate\Support\ServiceProvider;

class GeolibProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     * Bootstrap the application events.
     *
     * @return void
     */

    public function boot()
    {
        $messages = $this->getMessages();
        $this->app['validator']->extend(
            'latitude',
            'Agrodata\Geolib\Geolib@validateLatitude',
            $messages['latitude']
        );
        $this->app['validator']->extend(
            'longitude',
            'Agrodata\Geolib\Geolib@validateLongitude',
            $messages['longitude']
        );
        $this->app['validator']->extend(
            'dms',
            'Agrodata\Geolib\Geolib@validateDms',
            $messages['dms']
        );
    }

    protected function getMessages()
    {
        return [
            'latitude'            => 'O campo :attribute não é uma latitude válida.',
            'longitude'           => 'O campo :attribute não é uma longitude válida.',
            'dms'                 => 'O campo :attribute não é um dms válido.',
        ];
    }
}