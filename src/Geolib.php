<?php
namespace Agrodata\Geolib;

use Illuminate\Validation\Validator as BaseValidator;

class Geolib extends BaseValidator
{
    /**
    * Valida o formato do celular junto com o ddd
    * @param string $attribute
    * @param string $value
    * @return boolean
    */
    protected function validateLatitude($attribute, $value)
    {
        $parameters = include 'regex.php';
        $regex = $parameters['latitude'];

        return preg_match($regex, $value) > 0 && $value >= -90 && $value <= 90;
    }

    /**
    * Valida o formato do celular junto com o ddd
    * @param string $attribute
    * @param string $value
    * @return boolean
    */
    protected function validateLongitude($attribute, $value)
    {
        $parameters = include 'regex.php';
        $regex = $parameters['longitude'];

        return preg_match($regex, $value) > 0 && $value >= -180 && $value <= 180;
    }

    /**
    * Valida o formato do celular junto com o ddd
    * @param string $attribute
    * @param string $value
    * @return boolean
    */
    protected function validateDms($attribute, $value)
    {
        $parameters = include 'regex.php';
        $regex = $parameters['dms'];

        return preg_match($regex, $value) > 0;
    }
}