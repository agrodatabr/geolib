# Laravel GEOLIB

Este pacote foi criado para realizar a validação de latitude/longitude para API Laravel.


# Instalação

### Laravel 

Navegue até a pasta do seu projeto, por exemplo:

```
cd /etc/www/projeto
```

E então execute:

```
composer require agrodata/geolib
```

# Laravel app

Adicione em `config\app.php`.

```
use Agrodata\Geolib\GeolibProvider;
```

```
 'providers' => [
    /**
    * Geolib provider
    */
    GeolibProvider::class,
  ]
```

Agora, para utilizar a validação, basta fazer o procedimento padrão do `Laravel`.

A diferença é que será possível usar os seguintes métodos de validação:

* **`latitude`** - Valida se o campo está no formato (**`-99.999999`**)

* **`longitude`** - Valida se o campo está no formato (**`-99.999999`**)

* **`dms`** - Valida se o campo está no formato (**`99° 99′ 99″ a, 999° 99′ 99″ a`**)